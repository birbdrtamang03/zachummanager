<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->integer('phoneNo')->unique();
            $table->string('password');
        });
         // Add the default admin user
         \DB::table('manager')->insert([
            'name' => 'Manager',
            'phoneNo' => 17620190,
            'email' => 'manager@gmail.com',
            'password' => Hash::make('manager@123'), // Hash the password
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
