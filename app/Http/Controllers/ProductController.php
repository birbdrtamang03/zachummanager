<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Events;

class ProductController extends Controller
{
    // zachum controller below this
    public function zdashboard(){
        $items = Menu::orderBy('menu_id')->get();
        return view('admindashboard',compact('items'));
    }

    public function zcreatemenu(){
        return view('createmenu');
    }

    // create menu 
    function menuPost(Request $request){
        $request->validate([
            'name'=>'required',
            'description'=>'required',
            'type'=>'required',
            'price'=>'required',
            'image'=>'required',
            'status'=>'required',
        ]); 

        // dd($request->name);

        $filename = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move('menu_img',$filename);

        $data['menu_name'] = $request->name;
        $data['description'] = $request->description;
        $data['type'] = $request->type;
        $data['price'] = $request->price;
        $data['image'] = $filename;
        $data['status'] = $request->status;

        $menu = Menu::create($data);
        // dd($menu);
        if($menu){
            return redirect()->back()->with("success","The item added successfully!");
        }else{
            return redirect()->back()->with("error","Failed to add item. Please try again.");
        }
    }


    public function zevents(){
        $events = Events::orderBy('event_id')->get();
        return view('events',compact('events'));
    }

    public function zcreateEvent(){
        return view('createEvent');
    }

    function eventPost(Request $request){
        $request->validate([
            'eventTitle'=>'required',
            'description'=>'required',
            'image'=>'required',
        ]); 

        // dd($request->name);
        $image= file_get_contents($request->file('image')->getRealPath());

        // $filename = time().'.'.$request->image->getClientOriginalExtension();
        // $request->image->move('event_img',$filename);

        $data['event_name'] = $request->eventTitle;
        $data['description'] = $request->description;
        $data['image'] = $image;

        $event = Events::create($data);
        // dd($menu);
        if($event){
            return redirect()->back()->with("success","The event created successfully!");
        }else{
            return redirect()->back()->with("error","Failed to create event. Please try again.");
        }
    }
    
    public function zreservation(){
        return view('reservation');
    }

    public function zreservationPreview(){
        return view('reservationPreview');
    }
    public function zcustomers(){
        return view('customers');
    }
    public function zorders(){
        return view('orders');
    }
    public function zprofile(){
        return view('profile');
    }
}
