<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    function login(){
        if(Auth::check()){
            return redirect(route('zdashboard'));
        }
        return view('login');
    }
    function loginPost(Request $request){
        $request->validate([
            'email'=>'required',
            'password'=>'required'
        ]);
        $email = $request->input('email');

        $user = User::where('email', $email)->first();
        $credentials = $request->only('email','password');


        if(Auth::attempt($credentials)){
            $currentUser = $user->name;
            Session::put('currentUser', $currentUser);
            $customer = User::get();
            return redirect()->route('zdashboard')->with('success','Login Successful!');
        }
        if(!$user){
            return redirect()->route('login')->with("error", "Please enter the registered email.");
        }else{
            return redirect()->route('login')->with("error", "Please enter a correct password.");
        }
    }

      // logout
      function logout(){
        Session::flush();
        Auth::logout();
        return redirect(route('login'));
    }


}
