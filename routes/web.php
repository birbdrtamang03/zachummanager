<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', [AuthController::class, 'login'])->name('login');
Route::post('/', [AuthController::class, 'loginPost'])->name('login.post');

Route::group(['middleware' => 'auth'], function(){
    Route::get('/zdashboard',[ProductController::class, 'zdashboard'])->name('zdashboard');
    Route::get('/zcreatemenu',[ProductController::class, 'zcreatemenu'])->name('zcreatemenu');
    Route::post('/zcreatemenu', [ProductController::class, 'menuPost'])->name('createMenu.post');
    Route::get('/zevents',[ProductController::class, 'zevents'])->name('zevents');
    Route::get('/zcreateEvent',[ProductController::class, 'zcreateEvent'])->name('zcreateEvent');
    Route::post('/zcreateEvent', [ProductController::class, 'eventPost'])->name('createEvent.post');

    Route::get('/zreservation',[ProductController::class, 'zreservation'])->name('zreservation');
    Route::get('/zreservationPreview',[ProductController::class, 'zreservationPreview'])->name('zreservationPreview');
    Route::get('/zcustomers',[ProductController::class, 'zcustomers'])->name('zcustomers');
    Route::get('/zorders',[ProductController::class, 'zorders'])->name('zorders');
    Route::get('/zprofile',[ProductController::class, 'zprofile'])->name('zprofile');
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
});









